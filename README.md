# terraform-aws-dynamodb

Terraform module to provision a DynamoDB table with autoscaling.

Autoscaler scales up/down the provisioned OPS for the DynamoDB table based on the load.

## Usage

```
module "dynamodb" {
  source                       = "git::https://bitbucket.org/tomdevops/terraform-aws-dynamodb.git"
  stage                        = "${var.stage}"
  delimiter                    = "_"
  name                         = "${var.service}_table"
  hash_key                     = "HashKey"
  range_key                    = "RangeKey"
  autoscale_write_target       = 50
  autoscale_read_target        = 50
  autoscale_min_read_capacity  = 5
  autoscale_max_read_capacity  = 20
  autoscale_min_write_capacity = 5
  autoscale_max_write_capacity = 20
  enable_autoscaler            = "true"
}
```

## Advanced Usage
> With additional attributes, and global secondary indexes.
```
module "dynamodb" {
  source                        = "git::https://bitbucket.org/tomdevops/terraform-aws-dynamodb.git"
  stage                         = "${var.stage}"
  delimiter                     = "_"
  name                          = "${var.service}_docs"
  hash_key                      = "id"
  autoscale_write_target        = 50
  autoscale_read_target         = 50
  autoscale_min_read_capacity   = 5
  autoscale_max_read_capacity   = 20
  autoscale_min_write_capacity  = 5
  autoscale_max_write_capacity  = 20
  enable_autoscaler             = "true"
  enable_encryption             = "true"
  enable_point_in_time_recovery = "${var.dynamo_pitr}"

  dynamodb_attributes = [
    {
      name = "relationNumber"
      type = "S"
    },
    {
      name = "pegaReference"
      type = "S"
    },
    {
      name = "documentDate"
      type = "S"
    },
  ]

  global_secondary_index_map = [
    {
      name            = "RelationNumberGSI"
      hash_key        = "relationNumber"
      range_key       = "documentDate"
      read_capacity   = 5
      write_capacity  = 5
      projection_type = "ALL"
    },
    {
      name            = "PegaReferenceGSI"
      hash_key        = "pegaReference"
      range_key       = "documentDate"
      read_capacity   = 5
      write_capacity  = 5
      projection_type = "ALL"
    },
  ]
}
```

## Variables

| Name  | Default  | Description  | Required  |
|---|---|---|---|---|
| `namespace` | `""` | Namespace (e.g. `mkb` or `mkb-brandstof`) | No |
| `stage` | - | Stage (e.g. `prod`, `dev`, `staging`, `infra`) | Yes |
| `name`  | - | Name  (e.g. `app` or `cluster`) | Yes |
| `delimeter`  | `-` | Delimiter to be used between `namespace`, `stage`, `name`, and `attributes` | No |
| `attributes`  | `[]` | Additional attributes (e.g. `policy` or `role`) | No |
| `tags` | `{}` | Additional tags (e.g. map('BusinessUnit`,`XYZ`) | No |
| `autoscale_write_target` | `50` | The target value (in %) for DynamoDB write autoscaling | No |
| `autoscale_read_target` | `50` | The target value (in %) for DynamoDB read autoscaling | No |
| `autoscale_min_read_capacity` | `5` | DynamoDB autoscaling min read capacity | No |
| `autoscale_max_read_capacity` | `5` | DynamoDB autoscaling max read capacity | No |
| `autoscale_min_write_capacity` | `20` | DynamoDB autoscaling min write capacity | No |
| `autoscale_max_write_capacity`  ||||
| `enable_streams`  ||||
| `stream_view_type`  ||||
| `enable_encryption`  ||||
| `enable_point_in_time_recovery`  ||||
| `hash_key`  ||||
| `range_key`  ||||
| `ttl_attribute`  ||||
| `enable_ttl`  ||||
| `enable_autoscaler`  ||||
| `dynamodb_attributes`  ||||
| `global_secondary_index_map`  ||||

## Outputs

| Name  | Description  |
|---|---|
| `table_name` ||
| `table_id` ||
| `table_arn` ||
| `global_secondary_index_names` ||
| `table_stream_arn` ||
| `table_stream_label` ||

~ the end